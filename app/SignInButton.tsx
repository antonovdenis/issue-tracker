"use client";

import { Button } from "@radix-ui/themes";
import { signIn } from "next-auth/react";
import { FC, useState } from "react";
import toast from "react-hot-toast";
import { Spinner } from "./components";

interface SignInButtonProps {}

const SignInButton: FC<SignInButtonProps> = ({}) => {
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const signInWithGoogle = async () => {
    setIsLoading(true);
    try {
      await signIn("google");
    } catch (err) {
      toast.error("Unexpected error with signing in!");
    }
  };

  return (
    <>
      <Button
        style={{ cursor: "pointer" }}
        onClick={signInWithGoogle}
        disabled={isLoading}
      >
        Sign In
        {isLoading ? <Spinner /> : null}
      </Button>
    </>
  );
};

export default SignInButton;
