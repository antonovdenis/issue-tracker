"use client";

import { Button, Text } from "@radix-ui/themes";
import { signOut } from "next-auth/react";
import { FC, useState } from "react";
import toast from "react-hot-toast";
import { Spinner } from "./components";

interface SignOutButtonProps {}

const SignOutButton: FC<SignOutButtonProps> = ({}) => {
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const signUserOut = async () => {
    setIsLoading(true);
    try {
      await signOut();
    } catch (err) {
      toast.error("Unexpected error with signing out!");
    }
  };

  return (
    <>
      <Button
        disabled={isLoading}
        style={{ cursor: "pointer" }}
        onClick={signUserOut}
      >
        <Text>Sign Out</Text>
        {isLoading ? <Spinner /> : null}
      </Button>
    </>
  );
};

export default SignOutButton;
