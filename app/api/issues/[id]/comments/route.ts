import authOptions from "@/app/auth/authOptions";
import { commentSchema } from "@/app/validationSchemas";
import prisma from "@/prisma/prismaClient";
import { getServerSession } from "next-auth";
import { NextRequest, NextResponse } from "next/server";

interface Props {
  params: { id: string };
}

export async function POST(request: NextRequest, { params: { id } }: Props) {
  const session = await getServerSession(authOptions);
  if (!session) {
    return NextResponse.json({}, { status: 401 });
  }

  const body = await request.json();

  const validation = commentSchema.safeParse(body);
  if (!validation.success) {
    return NextResponse.json(validation.error.format(), { status: 400 });
  }

  const user = await prisma.user.findUnique({
    where: {
      email: body.authorEmail,
    },
  });
  if (!user) {
    return NextResponse.json({ error: "Invalid user!" }, { status: 400 });
  }

  const issue = await prisma.issue.findUnique({
    where: {
      id: parseInt(id),
    },
  });
  if (!issue) {
    return NextResponse.json({ error: "Invalid issue!" }, { status: 400 });
  }

  const newComment = await prisma.comment.create({
    data: {
      issueId: issue.id,
      authorId: user.id,
      content: body.content,
    },
  });

  return NextResponse.json(newComment, { status: 201 });
}
