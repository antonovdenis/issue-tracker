"use client";

import { SessionProvider } from "next-auth/react";
import { FC, PropsWithChildren } from "react";

interface AuthProviderProps extends PropsWithChildren {}

const AuthProvider: FC<AuthProviderProps> = ({ children }) => {
  return <SessionProvider>{children}</SessionProvider>;
};

export default AuthProvider;
