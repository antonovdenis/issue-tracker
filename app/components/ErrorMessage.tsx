import { Text } from "@radix-ui/themes";
import { FC, PropsWithChildren } from "react";

interface ErrorMessageProps extends PropsWithChildren {}

const ErrorMessage: FC<ErrorMessageProps> = ({ children }) => {
  return children ? (
    <Text color="red" as="p" className="text-xs">
      {children}
    </Text>
  ) : null;
};

export default ErrorMessage;
