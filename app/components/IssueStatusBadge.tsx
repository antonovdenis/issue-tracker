import { Status } from "@prisma/client";
import { Badge } from "@radix-ui/themes";
import { FC } from "react";

interface IssueStatusBadgeProps {
  status: Status;
}

const statusMap: Record<
  Status,
  { label: string; color: "red" | "violet" | "green" }
> = {
  OPEN: { label: "Open", color: "red" },
  IN_PROGRESS: { label: "In Progress", color: "violet" },
  CLOSED: { label: "Closed", color: "green" },
};

const IssueStatusBadge: FC<IssueStatusBadgeProps> = ({ status }) => {
  const map = statusMap[status];
  return <Badge color={map.color}>{map.label}</Badge>;
};

export default IssueStatusBadge;
