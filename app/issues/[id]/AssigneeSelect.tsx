"use client";

import { Skeleton } from "@/app/components";
import { Issue, User } from "@prisma/client";
import { Select } from "@radix-ui/themes";
import { useQuery } from "@tanstack/react-query";
import axios from "axios";
import { useRouter } from "next/navigation";
import { FC } from "react";
import toast, { Toaster } from "react-hot-toast";

const UNASSIGNED = "unassigned";

interface AssigneeSelectProps {
  issue: Issue;
}

const AssigneeSelect: FC<AssigneeSelectProps> = ({ issue }) => {
  const router = useRouter();
  const { data: users, error, isLoading } = useUsers();

  if (isLoading) return <Skeleton height="2rem" />;

  if (error) return null;

  const assignIssue = async (userId: string) => {
    try {
      await axios.patch("/api/issues/" + issue.id, {
        assignedToUserId: userId === UNASSIGNED ? null : userId,
      });
      router.refresh();
    } catch (error) {
      toast.error("Changes could not be saved!");
    }
  };

  return (
    <>
      <Select.Root
        defaultValue={issue.assignedToUserId || UNASSIGNED}
        onValueChange={assignIssue}
      >
        <Select.Trigger placeholder="Assign..." />
        <Select.Content>
          <Select.Group>
            <Select.Label>Suggestions</Select.Label>
            <Select.Item style={{ cursor: "pointer" }} value={UNASSIGNED}>
              Unassigned
            </Select.Item>
            {users?.map((user) => {
              return (
                <Select.Item
                  style={{ cursor: "pointer" }}
                  key={user.id}
                  value={user.id}
                >
                  {user.name}
                </Select.Item>
              );
            })}
          </Select.Group>
        </Select.Content>
      </Select.Root>
      <Toaster />
    </>
  );
};

const useUsers = () =>
  useQuery<User[]>({
    queryKey: ["users"],
    queryFn: () => axios.get("/api/users").then((res) => res.data),
    staleTime: 60 * 1000, //60s
    retry: 3,
  });

export default AssigneeSelect;
