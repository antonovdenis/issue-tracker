"use client";

import { Spinner } from "@/app/components";
import { Button, TextArea } from "@radix-ui/themes";
import axios from "axios";
import { useSession } from "next-auth/react";
import { useParams, useRouter } from "next/navigation";
import { FC, FormEvent, useState } from "react";
import toast from "react-hot-toast";

interface CommentFormProps {}

const CommentForm: FC<CommentFormProps> = ({}) => {
  const router = useRouter();
  const session = useSession();
  const { id: issueId } = useParams();

  const [comment, setComment] = useState("");
  const [isCommenting, setIsCommenting] = useState(false);

  const submitComment = async (event: FormEvent) => {
    event.preventDefault();
    try {
      setIsCommenting(true);
      await axios.post(`/api/issues/${issueId}/comments`, {
        authorEmail: session.data?.user?.email,
        content: comment,
      });
    } catch (error) {
      toast.error("Comment could not be sent!");
    }

    setIsCommenting(false);
    setComment("");
    router.refresh();
  };

  return session.data ? (
    <form onSubmit={submitComment}>
      <TextArea
        name="comment"
        placeholder="Add a comment..."
        value={comment}
        onChange={(e) => {
          setComment(e.target.value);
        }}
      />
      <Button disabled={isCommenting} mt="3" style={{ cursor: "pointer" }}>
        Comment
        {isCommenting && <Spinner />}{" "}
      </Button>
    </form>
  ) : null;
};

export default CommentForm;
