import prisma from "@/prisma/prismaClient";
import { Issue } from "@prisma/client";
import {
  Avatar,
  Blockquote,
  Card,
  Flex,
  Heading,
  Text,
} from "@radix-ui/themes";
import { formatDistance } from "date-fns";
import { FC } from "react";
import dynamic from "next/dynamic";

const CommentForm = dynamic(() => import("./CommentForm"), { ssr: false });

interface CommentSectionProps {
  issue: Issue;
}

const CommentSection: FC<CommentSectionProps> = async ({ issue }) => {
  const comments = await prisma.comment.findMany({
    where: {
      issueId: issue.id,
    },
    include: {
      author: true,
    },
  });

  return (
    <Flex direction="column" gap="3">
      <Heading mt="4" size="4">
        Comments
      </Heading>
      <CommentForm />
      {comments.length ? (
        comments.map((c) => (
          <Card key={c.id}>
            <Flex direction="column" gap="2">
              <Flex justify="between" align="center">
                <Flex gap="2" align="end">
                  <Avatar
                    size="1"
                    radius="full"
                    src={c.author.image!}
                    fallback="?"
                  />
                  <Text size="3">{c.author.name}</Text>
                </Flex>
                <Text size="2">
                  {formatDistance(new Date(c.createdAt), new Date(), {
                    addSuffix: true,
                  })}
                </Text>
              </Flex>
              <Blockquote mt="2">{c.content}</Blockquote>
            </Flex>
          </Card>
        ))
      ) : (
        <Text>No comments yet...</Text>
      )}
    </Flex>
  );
};

export default CommentSection;
