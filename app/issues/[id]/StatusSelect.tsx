"use client";

import { Issue, Status } from "@prisma/client";
import { Select, Text } from "@radix-ui/themes";
import axios from "axios";
import { useRouter } from "next/navigation";
import { FC } from "react";
import toast from "react-hot-toast";

interface StatusSelectProps {
  issue: Issue;
}

const StatusSelect: FC<StatusSelectProps> = ({ issue }) => {
  const router = useRouter();

  const statuses: { label: string; value: Status }[] = [
    { label: "Open", value: "OPEN" },
    { label: "Closed", value: "CLOSED" },
    { label: "In Progress", value: "IN_PROGRESS" },
  ];

  const changeStatus = async (status: Status) => {
    try {
      await axios.patch("/api/issues/" + issue.id, {
        status,
      });
      router.refresh();
    } catch (error) {
      toast.error("Changes could not be saved!");
    }
  };

  return (
    <Select.Root defaultValue={issue.status} onValueChange={changeStatus}>
      <Select.Trigger placeholder="Change status..." />
      <Select.Content>
        <Select.Group>
          <Select.Label>Statuses</Select.Label>
          {statuses.map((status) => (
            <Select.Item
              style={{ cursor: "pointer" }}
              key={status.value}
              value={status.value}
            >
              {status.label}
            </Select.Item>
          ))}
        </Select.Group>
      </Select.Content>
    </Select.Root>
  );
};

export default StatusSelect;
