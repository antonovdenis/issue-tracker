import authOptions from "@/app/auth/authOptions";
import prisma from "@/prisma/prismaClient";
import { Box, Flex, Grid } from "@radix-ui/themes";
import { getServerSession } from "next-auth";
import { notFound } from "next/navigation";
import { FC, cache } from "react";
import AssigneeSelect from "./AssigneeSelect";
import CommentSection from "./CommentSection";
import DeleteIssueButton from "./DeleteIssueButton";
import EditIssueButton from "./EditIssueButton";
import IssueDetails from "./IssueDetails";
import StatusSelect from "./StatusSelect";
import { Issue } from "@prisma/client";

interface IssueDetailPageProps {
  params: { id: string };
}

const fetchIssue = cache((issueId: number) =>
  prisma.issue.findUnique({
    where: { id: issueId },
  })
);

const IssueDetailPage: FC<IssueDetailPageProps> = async ({
  params: { id },
}) => {
  const session = await getServerSession(authOptions);

  if (Number.isNaN(Number(id))) notFound();

  const issue = await fetchIssue(parseInt(id));

  if (!issue) notFound();

  return (
    <Grid columns={{ initial: "1", sm: "5" }} gap="5">
      <Flex direction="column" gap="2" className="md:col-span-4">
        {/* <Box className="md:col-span-4"> */}
        <IssueDetails issue={issue} />
        <CommentSection issue={issue} />
        {/* </Box> */}
      </Flex>
      {session && (
        <Flex direction="column" gap="4">
          <AssigneeSelect issue={issue} />
          <StatusSelect issue={issue} />
          <EditIssueButton issueId={issue.id} />
          <DeleteIssueButton issueId={issue.id} />
        </Flex>
      )}
    </Grid>
  );
};

export default IssueDetailPage;

export async function generateMetadata({ params }: IssueDetailPageProps) {
  const issue = await fetchIssue(parseInt(params.id));

  return {
    title: `Issue Tracker | ${issue?.title}`,
    description: "Details of issue " + issue?.id,
  };
}
