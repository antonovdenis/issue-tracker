import { Button, Flex } from "@radix-ui/themes";
import Link from "next/link";
import { FC } from "react";
import IssueStatusFilter from "./IssueStatusFilter";
import IssueSearch from "./IssueSearch";

interface IssueActionsProps {}

const IssueActions: FC<IssueActionsProps> = ({}) => {
  return (
    <div className="flex flex-col md:flex-row space-y-3 md:space-y-0 justify-between">
      <IssueStatusFilter />
      <IssueSearch />
      <Button>
        <Link href="/issues/new">New Issue</Link>
      </Button>
    </div>
  );
};

export default IssueActions;
