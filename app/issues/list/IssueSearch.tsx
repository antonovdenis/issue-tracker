"use client";

import { MagnifyingGlassIcon } from "@radix-ui/react-icons";
import { Flex, TextField } from "@radix-ui/themes";
import { useRouter, useSearchParams } from "next/navigation";
import { ChangeEvent, FC } from "react";

interface IssueSearchProps {}

const IssueSearch: FC<IssueSearchProps> = ({}) => {
  const router = useRouter();
  const searchParams = useSearchParams();

  const onChange = (event: ChangeEvent<HTMLInputElement>) => {
    const search = event.target.value;
    const params = new URLSearchParams();

    if (search) params.append("search", search);

    const status = searchParams.get("status");
    if (status) params.append("status", status);

    const orderBy = searchParams.get("orderBy");
    if (orderBy) params.append("orderBy", orderBy);

    const direction = searchParams.get("direction");
    if (direction) params.append("direction", direction);

    const query = params.size ? "?" + params.toString() : "";
    router.push("/issues/list" + query);
  };

  return (
    <TextField.Root style={{ minWidth: "50%" }}>
      <Flex align="center" gap="1" mx="2" className="w-full">
        <MagnifyingGlassIcon width="25" height="25" color="black" />
        <TextField.Input
          style={{ outline: "none" }}
          defaultValue={searchParams.get("search") || ""}
          onChange={onChange}
          placeholder="Search for issues..."
        />
      </Flex>
    </TextField.Root>
  );
};

export default IssueSearch;
