import { IssueStatusBadge } from "@/app/components";
import { Issue, Status } from "@prisma/client";
import { TriangleDownIcon, TriangleUpIcon } from "@radix-ui/react-icons";
import { Flex, Table } from "@radix-ui/themes";
import { default as Link, default as NextLink } from "next/link";
import { FC } from "react";

export interface IssueQuery {
  status: Status;
  orderBy: keyof Issue;
  direction: "asc" | "desc";
  page: string;
  search: string;
}

interface IssueTableProps {
  searchParams: IssueQuery;
  issues: Issue[];
}

const IssueTable: FC<IssueTableProps> = ({ searchParams, issues }) => {
  return (
    <Table.Root variant="surface">
      <Table.Header>
        <Table.Row>
          {columns.map((col) => (
            <Table.ColumnHeaderCell key={col.value} className={col.className}>
              <Flex align="center" gap="2">
                <NextLink
                  href={{
                    query: {
                      ...searchParams,
                      orderBy: col.value,
                      direction:
                        searchParams.direction === "asc" ? "desc" : "asc",
                    },
                  }}
                >
                  {col.label}
                </NextLink>
                {col.value === searchParams.orderBy &&
                  (searchParams.direction === "asc" ? (
                    <TriangleUpIcon className="inline w-5 h-5" />
                  ) : (
                    <TriangleDownIcon className="inline w-5 h-5" />
                  ))}
              </Flex>
            </Table.ColumnHeaderCell>
          ))}
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {issues.map((issue) => (
          <Table.Row key={issue.id}>
            <Table.Cell width="40%">
              <Link href={`/issues/${issue.id}`}>{issue.title}</Link>
              <div className="block md:hidden">
                <IssueStatusBadge status={issue.status} />
              </div>
            </Table.Cell>
            <Table.Cell width="25%" className="hidden md:table-cell ">
              <IssueStatusBadge status={issue.status} />
            </Table.Cell>
            <Table.Cell width="25%" className="hidden md:table-cell">
              {issue.createdAt.toDateString()}
            </Table.Cell>
          </Table.Row>
        ))}
      </Table.Body>
    </Table.Root>
  );
};

const columns: {
  label: string;
  value: keyof Issue;
  className?: string;
}[] = [
  { label: "Issue", value: "title" },
  { label: "Status", value: "status", className: "hidden md:table-cell" },
  { label: "Created", value: "createdAt", className: "hidden md:table-cell" },
];

export const columnNames = columns.map((c) => c.value);

export default IssueTable;
