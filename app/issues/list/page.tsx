import Pagination from "@/app/components/Pagination";
import prisma from "@/prisma/prismaClient";
import { Status } from "@prisma/client";
import { FC } from "react";
import IssueActions from "./IssueActions";
import IssueTable, { IssueQuery, columnNames } from "./IssueTable";
import { Flex } from "@radix-ui/themes";
import { Metadata } from "next";

interface IssuesPageProps {
  searchParams: IssueQuery;
}

const IssuesPage: FC<IssuesPageProps> = async ({ searchParams }) => {
  const { search, status, orderBy, page, direction } = searchParams;

  const statuses = Object.values(Status);
  const validatedStatus = statuses.includes(status) ? status : undefined;

  const where = {
    title: { contains: search },
    status: validatedStatus,
  };

  const validatedOrderBy = columnNames.includes(orderBy)
    ? direction === "asc" || direction === "desc"
      ? { [orderBy]: direction }
      : undefined
    : undefined;

  const validatedPage = parseInt(page) || 1;
  const pageSize = 10;

  const issues = await prisma.issue.findMany({
    where,
    orderBy: validatedOrderBy,
    skip: (validatedPage - 1) * pageSize,
    take: pageSize,
  });

  const issueCount = await prisma.issue.count({ where });

  return (
    <Flex direction="column" gap="3">
      <IssueActions />
      <IssueTable searchParams={searchParams} issues={issues} />
      <Pagination
        currentPage={validatedPage}
        pageSize={pageSize}
        itemCount={issueCount}
      />
    </Flex>
  );
};

export const dynamic = "force-dynamic";
// export const revalidate = 0;

export default IssuesPage;

export const metadata: Metadata = {
  title: "Issue Tracker | Issues",
  description: "View all project issues",
};
