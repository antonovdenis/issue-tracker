import { Status } from "@prisma/client";
import z from "zod";

export const issueSchema = z.object({
  title: z.string().min(1, "Title is required").max(255),
  description: z.string().min(1, "Description is required").max(65535),
});

export const patchIssueSchema = z.object({
  title: z.string().min(1, "Title is required").max(255).optional(),
  description: z
    .string()
    .min(1, "Description is required")
    .max(65535)
    .optional(),
  assignedToUserId: z
    .string()
    .min(1, "assignedToUserId is required")
    .max(255)
    .optional()
    .nullable(),
  status: z.nativeEnum(Status).optional(),
});

export const commentSchema = z.object({
  authorEmail: z.string().email("email is required"),
  content: z.string().min(1, "content is required").max(65535),
});
